%ifndef tail
    %define tail 0
%endif 

%macro put 2
    %ifid %2
        %2: dq tail
        db %1, 0
        %define tail %2
    %else
        %warning "Second argument of colon macro is not a valid label."
    %endif
%endmacro

%macro colon 2
    %ifstr %1
        put %1, %2
    %endif
%endmacro