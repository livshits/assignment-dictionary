ASM=nasm
ASMFLAGS=-f elf64

all: dictionary

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o: dict.asm lib.o
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o: main.asm dict.o lib.o list.inc colon.inc lib.inc dict.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

dictionary: dict.o lib.o main.o
	ld -o $@ $^
