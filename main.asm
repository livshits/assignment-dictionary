%include "lib.inc"
%include "dict.inc"
%include "list.inc"

section .rodata
not_found_message: db "No key entry", 0xA, 0
prompt: db "Enter key: ", 0
invalid_key_message: db "Key cannot be empty or longer than 255 characters", 0xA, 0

section .text

global _start

%define buf_size 256
%define newline 0xA

_start:
    mov rdi, prompt
    call print_string


    sub rsp, buf_size
    mov rdi, rsp
    mov rsi, buf_size
    call read_line
    
    test rax, rax
    jz .invalid_key

    mov rdi, rsp
    mov rsi, tail
    call find_word
    test rax, rax
    jz .not_found

    push rax
    mov rdi, rax
    call string_length
    pop rdi
    add rdi, rax
    inc rdi

    call print_string
    call print_newline

    call exit

.not_found:
    mov rdi, not_found_message
    call print_string
    call exit

.invalid_key:
    mov rdi, invalid_key_message
    call print_string
    call exit

read_line:
   xor r8, r8
    mov r13, rdi
    mov r14, rsi
.read_loop:
    push rdi
    push rsi
    push r8
    call read_char

    pop r8
    pop rsi
    pop rdi

    cmp al, newline
    je .end_read
    mov [rdi], al

    inc r8
    inc rdi
    cmp r8, rsi
    jne .read_loop
    xor r8, r8

.end_read:
    mov byte [rdi], 0
    mov rax, r8
    ret